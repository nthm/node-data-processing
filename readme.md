# Data processing in Node

Processing pipelines for aggregating a series of large (~300 MB) JSON files.
Developed for interactive dashboards at Tutela.

Code is redacted. It's just reference material.

It's good to get comfortable with scripting in a language, whether that's
Python, Bash (or any shell), or anything that can be written quickly. For me,
that's Node because I'm most comfortable with ECMAScript.

These files are notes to remind myself how to avoid Bash scripts, Jenkinsfiles,
or any other languages which would otherwise take away from the task at hand.
It's a waste of time to try and learn Bash/Fish/Zsh syntax for operations I'm
already familiar with in JS like `.trim()`/`.startsWith()`/`.contains()`/etc.

Node is fully capable of orchestrating shell commands. It's able to run shell
commands via child processes both synchronously or in parallel (without blocking
the event loop). IO streams (like stdout/stderr) can be piped together, similar
to how a shell uses `a | b`. Processes can be detached like a shell's `disown`.
Operations like reading or writing files and making network requests can all be
performed in Node too.

---

All scripts here make use of child processes to isolate memory and everything
else, but that's not necessary. If the program is in Node you might want to
create a wrapper script to initialize an environment and then pivot to it,
letting it take over from there. An example of this is `s3-deploy` which is a
Node CLI tool for uploading to S3. It takes a lot of arguments, and you're
supposed to write the command as an `npm run` script in a package.json, but
that can be uncomfortably long and hard to read. Instead, you can point the npm
script to a wrapper that invokes it:

```js
// call `s3-deploy` and make it think it was given CLI args:

// if deployment fails, the exit code will be 0 unless the error is rethrown
process.on('unhandledRejection', error => {
  console.error('capturing unhandled promise rejection')
  throw error
})

process.argv.push(...`
./build/**
--exclude ./build/**/*.map
--cwd ./build/
--region us-west-1
--bucket your-bucket-name
--private
--deleteRemoved
--index index.html
--distId E1C23...3U09E5
`.trim().split(/\s/))

// the /bin file for s3-deploy does exactly this after a `#!/usr/bin/node`
require('./node_modules/s3-deploy/dist/index')
```

---

These are synchronous, so they will not be run in parallel. I'm batching all
output in a buffer using `exec`. If you want commands to stream, use `spawn`
or `spawnSync` which emit events for output.

`execSync` internally uses `spawnSync` with a shell and input/output
redirection. The difference is that `execSync` will not return (and therefore
not output anything) until the child command is complete. The output buffer is
capped by default to 200MB. You can specify a timeout too.

It's nice, but only makes sense in pipelines like this when blocking is OK.
