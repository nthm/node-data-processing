# `./jq`

Learn you a jq for greater good!

## Dataset

Imagine a 250MB newline delimited JSON file like this:

```json
{"CreatedDate":"2018-06-02","Country":"Russian Federation","DeviceCount":2,"QoSTestCount":49,"Jitter":{"title":"Jitter","value":0,"count":0,"unit":"MILLISECONDS","statisticalOpertation":"AVERAGE"},"Latency":{"title":"Latency","value":0,"count":0,"unit":"MILLISECONDS","statisticalOpertation":"AVERAGE"},"PacketLoss":{"title":"Packet Loss","value":0,"count":0,"unit":"PERCENTAGE","statisticalOpertation":"AVERAGE"},"UploadThroughput":{"title":"Upload Throughput","value":0,"count":0,"unit":"KILO_BITS_PER_SECOND","statisticalOpertation":"AVERAGE"},"DownloadThroughput":{"title":"Download Throughput","value":0,"count":0,"unit":"KILO_BITS_PER_SECOND","statisticalOpertation":"AVERAGE"}}
{"CreatedDate":"2018-06-01","Country":"Saudi Arabia","DeviceCount":6,"QoSTestCount":119,"Jitter":{"title":"Jitter","value":0,"count":0,"unit":"MILLISECONDS","statisticalOpertation":"AVERAGE"},"Latency":{"title":"Latency","value":0,"count":0,"unit":"MILLISECONDS","statisticalOpertation":"AVERAGE"},"PacketLoss":{"title":"Packet Loss","value":0,"count":0,"unit":"PERCENTAGE","statisticalOpertation":"AVERAGE"},"UploadThroughput":{"title":"Upload Throughput","value":0,"count":0,"unit":"KILO_BITS_PER_SECOND","statisticalOpertation":"AVERAGE"},"DownloadThroughput":{"title":"Download Throughput","value":0,"count":0,"unit":"KILO_BITS_PER_SECOND","statisticalOpertation":"AVERAGE"}}
{"CreatedDate":"2018-06-02","Country":"Canada","DeviceCount":1,"QoSTestCount":46,"Jitter":{"title":"Jitter","value":0,"count":0,"unit":"MILLISECONDS","statisticalOpertation":"AVERAGE"},"Latency":{"title":"Latency","value":0,"count":0,"unit":"MILLISECONDS","statisticalOpertation":"AVERAGE"},"PacketLoss":{"title":"Packet Loss","value":0,"count":0,"unit":"PERCENTAGE","statisticalOpertation":"AVERAGE"},"UploadThroughput":{"title":"Upload Throughput","value":0,"count":0,"unit":"KILO_BITS_PER_SECOND","statisticalOpertation":"AVERAGE"},"DownloadThroughput":{"title":"Download Throughput","value":0,"count":0,"unit":"KILO_BITS_PER_SECOND","statisticalOpertation":"AVERAGE"}}
{"CreatedDate":"2018-06-01","Country":"United Arab Emirates","DeviceCount":3,"QoSTestCount":87,"Jitter":{"title":"Jitter","value":0,"count":0,"unit":"MILLISECONDS","statisticalOpertation":"AVERAGE"},"Latency":{"title":"Latency","value":0,"count":0,"unit":"MILLISECONDS","statisticalOpertation":"AVERAGE"},"PacketLoss":{"title":"Packet Loss","value":0,"count":0,"unit":"PERCENTAGE","statisticalOpertation":"AVERAGE"},"UploadThroughput":{"title":"Upload Throughput","value":0,"count":0,"unit":"KILO_BITS_PER_SECOND","statisticalOpertation":"AVERAGE"},"DownloadThroughput":{"title":"Download Throughput","value":0,"count":0,"unit":"KILO_BITS_PER_SECOND","statisticalOpertation":"AVERAGE"}}
{"CreatedDate":"2018-06-01","Country":"Zimbabwe","DeviceCount":1,"QoSTestCount":1,"Jitter":{"title":"Jitter","value":0,"count":0,"unit":"MILLISECONDS","statisticalOpertation":"AVERAGE"},"Latency":{"title":"Latency","value":0,"count":0,"unit":"MILLISECONDS","statisticalOpertation":"AVERAGE"},"PacketLoss":{"title":"Packet Loss","value":0,"count":0,"unit":"PERCENTAGE","statisticalOpertation":"AVERAGE"},"UploadThroughput":{"title":"Upload Throughput","value":0,"count":0,"unit":"KILO_BITS_PER_SECOND","statisticalOpertation":"AVERAGE"},"DownloadThroughput":{"title":"Download Throughput","value":0,"count":0,"unit":"KILO_BITS_PER_SECOND","statisticalOpertation":"AVERAGE"}}
{"CreatedDate":"2018-06-02","Country":"Canada","DeviceCount":2,"QoSTestCount":32,"Jitter":{"title":"Jitter","value":0,"count":0,"unit":"MILLISECONDS","statisticalOpertation":"AVERAGE"},"Latency":{"title":"Latency","value":0,"count":0,"unit":"MILLISECONDS","statisticalOpertation":"AVERAGE"},"PacketLoss":{"title":"Packet Loss","value":0,"count":0,"unit":"PERCENTAGE","statisticalOpertation":"AVERAGE"},"UploadThroughput":{"title":"Upload Throughput","value":0,"count":0,"unit":"KILO_BITS_PER_SECOND","statisticalOpertation":"AVERAGE"},"DownloadThroughput":{"title":"Download Throughput","value":0,"count":0,"unit":"KILO_BITS_PER_SECOND","statisticalOpertation":"AVERAGE"}}
```

## Learn by example:

_Note_: Use all of these with `--slurp` since it's newline delimited JSON. If
using a vscode extension then you must reformat the JSON to be valid.

Filter JSON to only those with CreatedDate and Country to specific values, then
pipe that to a reducer that adds:
```js
map(select(.CreatedDate == "2018-06-01" and .Country == "Brazil"))
| reduce .[].DeviceCount as $item (0; . + $item)
```

Simplify the JSON objects to just Date/Country/Count by rebuilding them. Then
group by country:
```js
[.[] | { Date: .CreatedDate, Country, Count: .DeviceCount }]
| group_by(.Country)
```

Note that `[.[] | { ... }]` is exactly the same as `map({ ... })` as mentioned
here: https://github.com/stedolan/jq/issues/684
```js
map({ Date: .CreatedDate, Country, Count: .DeviceCount })
| group_by(.Country)
```

Now, if you run that you'll see nested arrays (those are the groups generated by
group_by). All the objects in that group will have the same country. Map each
group into a new object of the same country but adding all the counts:
```js
map({ Date: .CreatedDate, Country, Count: .DeviceCount })
| group_by(.Country)
| map({ Country: .[0].Country, Count: map(.Count) | add })
```
```json
[
  {
    "Country": "Afghanistan",
    "Count": 7
  },
  {
    "Country": "Argentina",
    "Count": 592
  },
  ...
```

But we want grouped by the date first, then then by country. The above is
grouped by country adding all days in the JSON (which would work if the JSON was
only one day but we're not there yet):
```js
map({
  "Date": .CreatedDate,
  "Country",
  "Count": .DeviceCount
})
| group_by(.Date) | map({
  "Date": .[0].Date,
  "RecordCount": length,
  "Countries": (group_by(.Country) | map({
    "Country": .[0].Country,
    "Count": map(.Count) | add
  }))
})
```
```json
[
  {
    "Date": "2018-06-01",
    "RecordCount": 106,
    "Countries": [
      {
        "Country": "Afghanistan",
        "Count": 5
      },
      {
        "Country": "Argentina",
        "Count": 289
      },
      {
        "Country": "Australia",
        "Count": 4
      },
      ...
  {
    "Date": "2018-06-02",
    "RecordCount": 81,
    "Countries": [
      {
        "Country": "Afghanistan",
        "Count": 2
      },
      {
        "Country": "Argentina",
        "Count": 303
      },
      ...
```

That's a way better way of crunching the data.

Add QoS test data, and remove the first renaming map:
```js
group_by(.CreatedDate) | map({
  "Date": .[0].CreatedDate,
  "RecordCount": length,
  "Countries": (group_by(.Country) | map({
    "Country": .[0].Country,
    "DeviceCount": map(.DeviceCount) | add,
    "QoSTestCount": map(.QoSTestCount) | add
  }))
})
```

---

This gets closer to what Pond needs, which is more CSV styled. But the list of
countries is not global - it's only for that one day.

```js
group_by(.CreatedDate) | map([
  .[0].CreatedDate,
  length,
  (group_by(.Country) | map(.[].Country))[]
])
```
```json
[
  [
    "2018-07-01",
    10,
    "Croatia",
    "Czech Republic",
    "Finland",
    "France",
    "Montenegro",
    "Nigeria",
    "Russian Federation",
    "South Africa",
    "United Kingdom",
    "United States"
  ],
  [
    "2018-07-02",
    9,
    "Australia",
    "Canada",
    "China",
    ...
```

---

Another one... This prints a list of objects that has a country (unique), the
number of total records for that country, and the dates for which that country
experienced events:
```js
map({
  "Date": .CreatedDate,
  "Country",
  "Count": .QoSTestCount
})
| group_by(.Country)[]
| {
  "Country": .[0].Country,
  "Records": length,
  "UniqueDates": [ .[].Date ] | unique /* try adding `| length` too */
}
```
```json
{
  "Country": "South Africa",
  "Records": 3,
  "UniqueDates": 2
}
{
  "Country": "Spain",
  "Records": 7,
  "UniqueDates": 2
}
{
  "Country": "Tajikistan",
  "Records": 1,
  "UniqueDates": 1
}
{
  "Country": "United Kingdom",
  "Records": 4,
  "UniqueDates": 2
}
```