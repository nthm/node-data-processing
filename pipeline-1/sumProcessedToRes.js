const fs = require('fs-extra')
const path = require('path')
const minimist = require('minimist')

const argv = minimist(process.argv.slice(2))

const isoDate = date => date.toISOString().split('T')[0]
const validDate = date => isNaN(Date.parse(date)) === false
const noJSONExt = filename => filename.split('.json')[0]

const processedDir = path.resolve(__dirname, '../build/data/processed')
// can easily sort ISO dates
const processedFiles = fs.readdirSync(processedDir).sort()

// fallback values if not told a date range to process
let startDate = noJSONExt(processedFiles[0])
let endDate = noJSONExt(processedFiles[processedFiles.length - 1])

// be less forgiving than start.js. the added complexity is to be flexible with
// date ranges since the output files are written into the frontend's tree to be
// picked up by webpack (frontend not uploaded in this repo, sorry)

let startThreshold = -Infinity
if (('from' in argv) && validDate(argv.from)) {
  console.log('ISO date --from given. Limiting data range')
  const date = new Date(argv.from)
  startThreshold = date.getTime()
  startDate = isoDate(date)
}
let endThreshold = Infinity
if (('to' in argv) && validDate(argv.to)) {
  console.log('ISO date --to given. Limiting data range')
  const date = new Date(argv.to)
  endThreshold = date.getTime()
  endDate = isoDate(date)
}

console.log()

// there are three files to export as Pond format. one counts and tests, another
// places to counts, and then sidebar values
const oneCounts = {
  'name': 'byCount',
  'utc': true,
  'columns': [
    'time',
    'Count',
    'Tests',
  ],
  'points': [],
}

// all places ever encountered
const places = new Set()

// key: place, value: count, as a total. this is used for knowing what places to
// keep (if their average is greater than a threshold)
const placeSums = new Map()

// key: timestamp, value: object of (key: place, value: count)
// example: [152717...02, { 'Work': 201828, 'Home': ... }]
const timestampToPlaceCountPair = new Map()

const placeCounts = {
  'name': 'byPlace',
  'utc': true,
  'columns': [
    'time',
    // all other places
  ],
  'points': [],
}

// used by type counts
const dayBeforeEndDate = (() => {
  const date = new Date(endDate)
  date.setDate(date.getDate() - 1)
  return isoDate(date)
})()

let typeCountLastDay = null
let typeCountSecondLastDay = null

let previousDate = null
for (const filename of processedFiles) {
  const fileISO = noJSONExt(filename)
  const currentDate = new Date(fileISO)
  const currentTimestamp = currentDate.getTime()

  if (previousDate !== null) {
    const isoPrevious = isoDate(previousDate)
    // move `previous` up. hopefully to `current`, but maybe less
    previousDate.setDate(previousDate.getDate() + 1)
    if (isoDate(previousDate) !== fileISO) {
      console.log(`Warning: Data gap between ${isoPrevious} and ${fileISO}`)
    }
  } else {
    console.log('Starting at', fileISO)
  }
  // move `previous` up to `current`
  previousDate = currentDate
  if (currentTimestamp < startThreshold) {
    // gaps in data are still reported, but otherwise there's silence
    continue
  }
  if (currentTimestamp > endThreshold) {
    console.log(filename, 'is beyond is the end date. Not processing. Exiting.')
    break
  }
  console.log(fileISO)
  const data = require(path.join(processedDir, filename))

  oneCounts.points.push([
    currentTimestamp,
    data.One.Count,
    data.One.Tests,
  ])

  // handle all `Places`
  for (const place in data.Places) {
    places.add(place)

    // only handle `Count` for now. drop `Tests`
    const value = data.Places[place].Count

    placeSums.set(place, (placeSums.get(place) || 0) + value)

    const mapEntry = timestampToPlaceCountPair.get(currentTimestamp)
    // insert
    if (!mapEntry) {
      timestampToPlaceCountPair.set(currentTimestamp, { [place]: value })
      continue
    }
    // else, update
    mapEntry[place] = (mapEntry[place] || 0) + value
    timestampToPlaceCountPair.set(currentTimestamp, mapEntry)
  }

  // possibly handle type counts
  if (fileISO === dayBeforeEndDate) {
    typeCountSecondLastDay =
      Object.values(data.Types).filter(obj => obj.Count > 0).length
  }
  if (fileISO === endDate) {
    typeCountLastDay =
      Object.values(data.Types).filter(obj => obj.Count > 0).length
  }
}

const days = timestampToPlaceCountPair.size
console.log()
console.log(`Processed ${days} days`)
console.log(`Accumulated ${placeSums.size} places`)
console.log()

// decide whether or not to include a place in the result. see if the average is
// below a threshold
const threshold = 20000
// https://stackoverflow.com/q/35940216/delete-from-set-or-map-during-iteration
placeSums.forEach((sum, place) => {
  if (sum / days < threshold) {
    placeSums.delete(place)
  }
})

console.log(`${placeSums.size} places have an average >= ${threshold}`)
console.log('Keeping those')
console.log()

const allFinalPlaces = [...placeSums.keys()]
placeCounts.columns.push(...allFinalPlaces)
for (const [timestamp, placeCountPair] of timestampToPlaceCountPair) {
  placeCounts.points.push([
    timestamp,
    ...allFinalPlaces.map(place => placeCountPair[place] || 0),
  ])
}

const outDir = path.resolve(__dirname, '../res/data')
console.log('Writing to', outDir)
const writeJSON = (name, data) =>
  fs.writeJSONSync(path.join(outDir, name), data, { spaces: 2 })

writeJSON('countAndTests.json', oneCounts)
writeJSON('countPerPlace.json', placeCounts)

// calculate sidebar values

const percentChange = (day, dayBefore) =>
  (((day - dayBefore) / dayBefore) * 100).toFixed(1)

// popping (see below) is destructive so do this reduction first:
const testsPerformed30Days =
  oneCounts.points.reduce((sum, arr) => sum + arr[2], 0)

const oneLastDay = oneCounts.points.pop()
const oneSecondLastDay = oneCounts.points.pop()

// while the other two files were meant to be given directly to Pond for
// graphing, this is to populate some statistics next to the charts. it's
// imported by webpack into a React component
writeJSON('sidebar.json', {
  'dateRange': `${startDate} to ${endDate}`,
  'generatedOn': (new Date()).toUTCString(),
  'stats': [
    // for: Unique Counts Yesterday
    {
      'value': oneLastDay[1],
      'change': percentChange(
        oneLastDay[1],
        oneSecondLastDay[1]
      ),
    },
    // for: Tests Performed Yesterday
    {
      'value': oneLastDay[2],
      'change': percentChange(
        oneLastDay[2],
        oneSecondLastDay[2]
      ),
    },
    // for: Tests Performed 30 Days
    {
      'value': testsPerformed30Days,
    },
    // for: Live Types Yesterday
    {
      'value': typeCountLastDay,
      'change': percentChange(
        typeCountLastDay,
        typeCountSecondLastDay
      ),
    },
  ],
})
