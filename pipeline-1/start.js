const minimist = require('minimist')
const { spawnSync } = require('child_process')

const argv = minimist(process.argv.slice(2))

const isoDate = date => date.toISOString().split('T')[0]

const date = new Date()

// we don't actually want one month - we want exactly 30 days
// FIXME: charts shows one day short :/ bump to 31
date.setDate(date.getDate() - 31)
const thirtyDaysFromYesterday = isoDate(date)
// don't use !('to' in argv) and minimist will turn `--to` into `{ to: true }`
if (!argv.from || argv.from === true) {
  console.log(`'--from' not provided. Setting to '${thirtyDaysFromYesterday}'`)
  argv.from = thirtyDaysFromYesterday
}

date.setDate(date.getDate() + 30)
const yesterday = isoDate(date)
if (!argv.to || argv.to === true) {
  console.log(`'--to' not provided. Setting to '${yesterday}'`)
  argv.to = yesterday
}

const sh = command => {
  const [binary, ...other] = command.split(/\s+/)
  // the { stdio: inherit } means bind the stdout and stderr to the parent
  // this is no longer the recommended way. see claspDeploy.js
  const child = spawnSync(binary, [...other], { stdio: 'inherit' })
  if (child.status !== 0) {
    process.exit(child.status)
  }
  return child
}

// note that process argv must be an array
sh(`npm run data:fetch -- --from ${argv.from} --to ${argv.to}`)
sh('npm run data:rawToProcessed')
sh(`npm run data:processedToRes -- --from ${argv.from} --to ${argv.to}`)

// sh(`npm run data:fetch -- --from ${argv.from} --to ${argv.to}`)
// sh(`npm run data:aggregate -- ${argv.from}_${argv.to}.json`)
