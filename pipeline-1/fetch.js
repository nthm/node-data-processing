const fs = require('fs-extra')
const path = require('path')
const minimist = require('minimist')
const fetch = require('node-fetch')
const AWS = require('aws-sdk')

process.on('unhandledRejection', (reason, promise) => {
  console.error('Uncaught error in', promise)
  console.error(reason)
  process.exit(1)
})

const argv = minimist(process.argv.slice(2), {
  default: {
    concurrentS3Requests: 10,
    // these depends on their respective DynamoDB throttles
    concurrentAPIOneRequests: 10,
    concurrentAPITwoRequests: 5,
    saveDir: path.resolve(__dirname, '../build/data/raw'),
  },
})

const bucketName = 'processed-cache'
const urlAPIOne = 'https://localhost/status/'
const urlAPITwo = 'https://localhost/metrics/'

// be less forgiving than start.js and less complex than sumProcessedToRes.js
if (!('from' in argv) || !('to' in argv)) {
  console.log('Specify both --to and --from as ISO dates')
  process.exit(1)
}

// ------------
// determine minimum downloads

console.log(`Date range: '${argv.from}' until '${argv.to}'`)

const isoDate = date => date.toISOString().split('T')[0]

const startDate = new Date(argv.from)
const stopDate = new Date(argv.to)

const wantedDates = []
while(startDate <= stopDate) {
  wantedDates.push(isoDate(startDate))
  startDate.setDate(startDate.getDate() + 1)
}

const rawDirectories = ['one', 'two']
const subDirs = {}
const missingDates = {}

// because of the way sumRawToProcessed works, if files exist in processed/ then
// they aren't needed in raw/

const processedDir = path.join(__dirname, '..', 'build', 'data', 'processed')
fs.ensureDirSync(processedDir)
const existingProcessedFiles = fs.readdirSync(processedDir)

// keep track of how much JSON has been written this run
let totalSize = 0
const niceSize = bytes => `${(bytes / 1e6).toFixed(2)} MB`

// define this seperately from the Tutela API ones since it's handled first
const requestQueueS3Processed = requestLimiter(argv.concurrentS3Requests)

// reduce the traffic to APIOne (which returns too much data - enough to hang
// itself). most of which is wasted, and the processed payload is a fraction of
// the size

const sts = new AWS.STS()
let s3 // = new AWS.S3()

// ------------
// main

sts.assumeRole({
  RoleArn: 'arn:aws:iam::accountnumber:role/rolename',
}).promise()
  .then(data => {
    console.log('Assumed role:', data)
    console.log()
    AWS.config.credentials = new AWS.Credentials(
      data.Credentials.AccessKeyId,
      data.Credentials.SecretAccessKey,
      data.Credentials.SessionToken)

    // it's now safe to set this
    s3 = new AWS.S3()
  })
  .then(() => s3.listObjectsV2({ Bucket: bucketName }).promise())
  .then(data => {
    const existingS3Files = data.Contents.map(item => item.Key)
    console.log('S3 has', existingS3Files.length, 'processed files')
    console.log()

    // NOTE: there might be files on local processed/ that aren't on S3.
    // whatever. leave that up to the sumRawToProcessed script

    const handledByS3 = wantedDates.filter(iso =>
      existingS3Files.includes(iso + '.json') &&
      !existingProcessedFiles.includes(iso + '.json'))

    console.log('Files not in processed/ that will be downloaded from S3:')
    console.log(handledByS3)
    console.log()

    const s3requests = handledByS3.map(iso => {
      // also, while at it, add them to processed/
      existingProcessedFiles.push(iso + '.json')
      console.log('Requesting', iso, 'from S3')

      return requestQueueS3Processed(requestS3, iso)
    })

    rawDirectories.forEach(name => {
      const subDir = path.join(argv.saveDir, name)
      fs.ensureDirSync(subDir)
      subDirs[name] = subDir

      const existingRawFiles = fs.readdirSync(subDir)
      missingFiles[name] =
        wantedDates.filter(iso =>
          !existingProcessedFiles.includes(iso + '.json') &&
          !existingRawFiles.includes(iso + '.json'))
    })

    console.log('Other unprocessed files, not in S3, to be downloaded:')
    console.log(JSON.stringify(missingFiles, null, 2))
    console.log()

    const requestQueueOne = requestLimiter(argv.concurrentAPIOneRequests)
    const requestsFromOne = missingDates.one.map(date =>
      requestQueueOne(requestTutelaAPI(urlAPIOne, subDirs.one), date))

    const requestQueueTwo = requestLimiter(argv.concurrentAPITwoRequests)
    const requestsFromTwo = missingDates.one.map(date =>
      requestQueueTwo(requestTutelaAPI(urlAPITwo, subDirs.two, {
        AuthToken: 'XYZ',
      }), date))

    // flatten the array of arrays into one promise
    return Promise.all(
      [s3requests, requestsFromOne, requestsFromTwo].map(requestArray =>
        Promise.all(requestArray)))
  })
  .then(() => {
    console.log('Total JSON written:', niceSize(totalSize))
  })

// ------------
// helper functions

// limit concurrent network requests to each API. lifted from `p-limit`
function requestLimiter(concurrency) {
  const queue = []
  let active = 0

  const next = () => {
    active--
    if (queue.length > 0) {
      queue.shift()()
    }
  }
  const run = (fn, resolve, ...args) => {
    active++
    const result = fn(...args)
    resolve(result)
    result.then(next, next)
  }
  const enqueue = (fn, resolve, ...args) => {
    if (active < concurrency) {
      run(fn, resolve, ...args)
    } else {
      queue.push(run.bind(null, fn, resolve, ...args))
    }
  }
  return (fn, ...args) => new Promise(resolve => enqueue(fn, resolve, ...args))
}

// fetch wrapper to factor out code and log timing information
function requestTutelaAPI(url, saveDir, headers = {}) {
  headers = Object.assign({
    Authorization: 'MagicToken',
  }, headers)

  // return a function to allow for currying
  return date => {
    const prettyTimeLog = `Fetch ${url.split('.com')[1]} ${date}`
    console.time(prettyTimeLog)
    const saveFile = path.join(saveDir, date + '.json')
    return fetch(`${url}?from=${date}&to=${date}`, { headers })
      .then(response => {
        if (!response.ok) {
          // this is a Buffer() that you can't read ^-^
          throw response
        }
        return response.json()
      })
      .then(data => {
        fs.writeJSONSync(saveFile, data, { spaces: 2 })
        const stats = fs.statSync(saveFile)
        totalSize += stats.size

        console.timeEnd(prettyTimeLog)
        console.log('JSON size:', niceSize(stats.size))
      })
      .catch(err => {
        console.error(`Request failed; "${date}"`)
        err.text().then(body => {
          throw body
        })
      })
  }
}

function requestS3(date) {
  const file = `${date}.json`
  return s3.getObject({
    Bucket: bucketName,
    Key: file,
  }).promise()
    .then(res => {
      console.log('S3 object', date, niceSize(res.ContentLength))
      totalSize += res.ContentLength
      const data = JSON.parse(res.Body.toString('utf-8'))
      fs.writeJSON(path.join(processedDir, file), data, { spaces: 2 })
    })
}
