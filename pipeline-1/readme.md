# 1

First pipeline, less factored than 2 but here for reference.

## File structure

The general directory layout for JSON was: `../build/{raw/{one,two},processed}/`
Where `one` and `two` were names of API endpoints. Each leaf folder had a list
of JSON files named as ISO dates (like 2018-07-06).

Raw files were very large; hundreds of MB. After processing it could be reduced
to 2-5MB.

## Scripts

Start reading at _start.js_, which calls other files.

### `start.js`

For CI servers to more easily run the pipeline periodically. Build parameters
needed to be conveyed to each script, and have sane fallbacks if not provided at
all. Jenkins doesn't let you have dynamic default parameters so it's handled in
that file instead.

### `fetch.js`

Pulls data from two APIs concurrently. Doesn't modify payloads beyond
prettifying JSON.

Diffs desired and existing directories
to determine the minimum amount of network requests needed. It then makes
those API requests concurrently using a promise queue

Pulling from the Tutela API was considered expensive; used +400MB a day. So, S3
was used as cache to pull from when possible. The raws aren't in S3, only
processed files. If they're up there, raws aren't requested. See _sumRawToProcessed.js_

### `sumRawToProcessed.js`

Raw files (from _fetch.js_) loaded from the two output directories are processed
and reduced and then written to `processed/`

Diffs three directories, determines how lazy it can be, does processing, and
then uploads results to S3.

### `sumProcessedToRes.js`

Should be called processed to Pond. It formats the JSON into a format the chart
library can understand.

Result is written to `res/` which is then picked up by webpack.

Resolves gaps in date ranges for files names in ISO date format.
