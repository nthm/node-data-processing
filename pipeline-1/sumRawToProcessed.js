const fs = require('fs-extra')
const path = require('path')
const crypto = require('crypto')
const AWS = require('aws-sdk')

process.on('unhandledRejection', (reason, promise) => {
  console.error('Uncaught error in', promise)
  console.error(reason)
  process.exit(1)
})

const baseDir = path.resolve(__dirname, '../build/data/')
const rawDir = path.join(baseDir, 'raw')
const processedDir = path.join(baseDir, 'processed')
fs.ensureDirSync(processedDir)

// considered an algorithm to list files in raw/one/, raw/two/, and processed/,
// and if looping the set of the raws discovers some files in one but not the
// other, then exit. however, it's far simpler to blindly take the set of the
// raws and let it throw when/if one doesn't exist

const subDirs = ['one', 'two'].map(x => path.join(rawDir, x))
const rawFiles = Array.from(new Set(...subDirs.map(x => fs.readdirSync(x))))

const processedFiles = fs.readdirSync(processedDir)
const queue = rawFiles.filter(file => !processedFiles.includes(file))

console.log('Missing files to be processed:')
console.log(queue)

const [onePath, twoPath] = subDirs
const processDay = rawFile => {
  const date = rawFile.split('.json')[0]
  console.log('Processing', date)
  const result = {
    Date: date,
    One: {
      Count: 0,
      Tests: 0,
    },
    Places: {},
    Types: {},
  }

  // looked into streaming the JSON since the payload used to be large (250 MB)
  // but now it's around 10 MB max, and not worth the complexity:
  // https://github.com/uhop/stream-json/issues/5

  const message = `Loading ${rawFile}`
  console.time(message)
  const onePayload = require(path.join(onePath, rawFile))
  const twoPayload = require(path.join(twoPath, rawFile))
  console.timeEnd(message)

  onePayload.forEach(obj => {
    result.One.Count += obj.Count
    result.One.Tests += obj.Tests
  })

  twoPayload.forEach(obj => {
    if (!(obj.Place in result.Places)) {
      result.Places[obj.Place] = {
        Count: 0,
        Tests: 0,
      }
    }
    result.Places[obj.Place].Count += obj.Count
    result.Places[obj.Place].Tests += obj.Tests

    // tpyes are referenced by "Name" in the API
    if (!(obj.Name in result.Types)) {
      result.Types[obj.Name] = {
        Count: 0,
        Tests: 0,
      }
    }
    result.Types[obj.Name].Count += obj.Count
    result.Types[obj.Name].Tests += obj.Tests
  })

  fs.writeJSONSync(
    path.join(processedDir, rawFile),
    result,
    { spaces: 2 }
  )

  // avoid an fs.readdirSync
  processedFiles.push(rawFile)
}

queue.forEach(processDay)

// upload the results to S3

const bucketName = 'processed-cache'

const sts = new AWS.STS()
let s3 // = new AWS.S3()

// in S3, an ETag is just the MD5 of the file contents
const hashFile = data => crypto.createHash('md5').update(data).digest('hex')

sts.assumeRole({
  RoleArn: 'arn:aws:iam::accountnumber:role/rolename',
}).promise()
  .then(data => {
    console.log('Assumed role:', data)
    AWS.config.credentials = new AWS.Credentials(
      data.Credentials.AccessKeyId,
      data.Credentials.SecretAccessKey,
      data.Credentials.SessionToken)

    // it's now safe to set this
    s3 = new AWS.S3()
  })
  .then(() => s3.listObjectsV2({ Bucket: bucketName }).promise())
  .then(data => {
    const keyToHashS3 = data.Contents.reduce((acc, item) => {
      acc[item.Key] = item.ETag
      return acc
    }, {})
    // now perform the upload
    processedFiles.forEach(filename => {
      fs.readFile(path.join(processedDir, filename)).then(content => {
        const hash = hashFile(content)
        if (!(filename in keyToHashS3) || keyToHashS3[filename] !== hash) {
          s3.putObject({
            Bucket: bucketName,
            ACL: 'private',
            Key: filename,
            Body: content,
          }, err => {
            console.log('Upload', filename, hash)
            if (err) console.error('Failed.', err)
          })
        }
      })
    })
  })
